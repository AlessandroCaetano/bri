#!/usr/bin/python3

########################################
## import packages
########################################
import csv

import logging

from tqdm import tqdm
from time import time

from nltk.corpus.reader import XMLCorpusView

########################################
## Configurations
########################################
logging.basicConfig(filename='processador.log',level=logging.DEBUG)

########################################
## reading configuration file
########################################
t0 = time()
with open('PC.cfg') as f:
	for row in f.readlines():
	    if row.replace('\n','').split("=")[0] == 'LEIA': 
		    reads = row.replace('\n','').split("=")[1]		
	    if row.replace('\n','').split("=")[0] == 'CONSULTAS': 
	        queries = row.replace('\n','').split("=")[1]		
	    if row.replace('\n','').split("=")[0] == 'ESPERADOS':
	        expected = row.replace('\n','').split("=")[1]

logging.info('Config file PC.cfg read in: %0.3fs' % (time() - t0))

QUERIES = XMLCorpusView(reads, 'FILEQUERY/QUERY')

########################################
## mounting  list with query number and text
########################################
query_list = []
expected_list = []

t0 = time()
for query in tqdm(QUERIES, desc='Processing CFQUERY'):
    query_number = query[0].text
    query_text = query[1].text.replace('\n', '').strip()
    query_list.append([query_number,query_text])
    records = query[3]
    for document in records:
        expected_list.append([query_number,document.text,document.attrib['score']])
        

logging.info('CFQuery XML file read in: %0.3fs' % (time() - t0))

########################################
## writing query list to a csv file
########################################
t0 = time()
with open(queries, 'w') as csvfile:
	out = csv.writer(csvfile, delimiter=';')
	out.writerow(['QueryNumber', 'QueryText'])
	for row in tqdm(query_list, desc='Writing Queries list'):
		out.writerow(row)
logging.info('Query CSV file writen in: %0.3fs' % (time() - t0))


########################################
## writing document list to a csv file
########################################
t0 = time()
with open(expected, 'w') as csvfile:
	out = csv.writer(csvfile, delimiter=';')
	out.writerow(['QueryNumber', 'QueryText'])
	for row in tqdm(expected_list, desc='Writing Expected list'):
		out.writerow(row)
logging.info('Document CSV file writen in: %0.3fs' % (time() - t0))