#!/usr/bin/python3

########################################
## import packages
########################################
import csv
import math

import logging

import pandas as pd

from tqdm import tqdm
from time import time

########################################
## Support Functions
########################################
def hasNumbers(inputString):
    return any(char.isdigit() for char in inputString)

def clearList(inputDict):
    cloneDict = inputDict.copy()
    for word, doc_number in tqdm(cloneDict.items(), desc='Cleaning Inverted List'):
        if len(str(word)) <= 2 or hasNumbers(str(word)):
            inputDict.pop(word)
    return inputDict 

def computeTFIDF(term, docTerms, docNumber, numberDocsWithTerm):
    
	number_terms_on_doc = 0
	for t in docTerms:
		if t == term:
			number_terms_on_doc += 1

	tf = number_terms_on_doc / len(docTerms)
	idf = math.log(docNumber / numberDocsWithTerm)

	return tf * idf

########################################
## Configurations
########################################
logging.basicConfig(filename='indexer.log',level=logging.DEBUG)


########################################
## reading configuration file
########################################
t0 = time()
with open('INDEX.cfg') as f:
	for row in f.readlines():
		if row.replace('\n','').split("=")[0] == 'LEIA': 
			idf_file = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'ESCREVA': 
			write_file = row.replace('\n','').split("=")[1]

logging.info('Config file INDEX.cfg read in: %0.3fs' % (time() - t0))

########################################
## Create inverted list
########################################
t0 = time()
inverted_list =  dict()
df = pd.read_csv(idf_file, header=None, delimiter=';')	

for index, row in tqdm(df.iterrows(), desc='Creating inverted list'):		
	word = row[0]		
	doc_number = row[1].replace('[','').replace(']','').replace("'",'').replace(' ','')
	inverted_list[word] = doc_number.split(',')

logging.info('Inverted list created in: %0.3fs' % (time() - t0))

t0 = time()
inverted_list = clearList(inverted_list)
logging.info('Inverted list cleaned in: %0.3fs' % (time() - t0))

########################################
## Preparations for TFIDF
########################################
term_list = list(inverted_list.keys())	

doc_terms = dict()
	
for term in inverted_list:
	docs_terms = inverted_list[term]

	for doc in docs_terms:	
		if doc in doc_terms:
			doc_terms[doc].append(term)
		else:
			buffer_list = list()
			buffer_list.append(term)
			doc_terms[doc] = buffer_list									
		
doc_list = list(doc_terms.keys())	
number_of_docs = len(doc_list)

########################################
## Calculating TFIDF
########################################
t0 = time()
term_doc_matrix = dict()	
for doc, doc_term in tqdm(doc_terms.items(), desc='Calculating TFIDF'):
	term_score = list()
	for term in term_list:
		number_of_docs_with_term = len(set(inverted_list[term]))			
		tfIdf = computeTFIDF(term, doc_term, number_of_docs, number_of_docs_with_term)	       	     
		term_score.append(tfIdf)

	term_doc_matrix[doc] = term_score

logging.info('TFIDF Calculated in: %0.3fs' % (time() - t0))

########################################
## Saving Files
########################################
t0 = time()
with open(write_file,"w") as f:
	writer = csv.writer(f,delimiter=';')
	for index, value in term_doc_matrix.items():
		if value != []:
			writer.writerow([index,value])

with open('term_list.csv',"w") as f:
	writer = csv.writer(f,delimiter=';')
	writer.writerow(term_list)

logging.info('CSV Files saved in: %0.3fs' % (time() - t0))
print('CSV Files saved in: %0.3fs' % (time() - t0))