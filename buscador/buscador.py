#!/usr/bin/python3

########################################
## import packages
########################################
import csv
import math
import nltk

import logging
import operator

import pandas as pd

from tqdm import tqdm
from time import time

from sklearn.metrics.pairwise import cosine_similarity
########################################
## Support Functions
########################################

def create_score_list(tokens, term_list) :
    score_list = []
		
    for term in term_list:
	    if term in tokens:
		    score_list.append(1)
	    else :
		    score_list.append(0)

    return score_list
	
def similaridadeCosseno(a,b):
    sumaa, sumab, sumbb = 0, 0, 0
    
    for i in range(len(a)):        
        x = float(a[i])
        y = float(b[i])
        
        sumaa += x*x
        sumbb += y*y
        sumab += x*y
    
    if math.sqrt(sumaa*sumbb) > 0:
    	return sumab/math.sqrt(sumaa*sumbb)
    else:
    	return 0

########################################
## Configurations
########################################
logging.basicConfig(filename='searcher.log',level=logging.DEBUG)


########################################
## Read config file
########################################

term_list_file = ""
results_file = ""
model_file = ""
queries_file = ""

t0 = time()
with open('BUSCA.cfg', 'r') as f:
	for row in f.readlines():
		if row.replace('\n','').split("=")[0] == 'MODELO': 
			model_file = row.replace('\n','').split("=")[1]		
		if row.replace('\n','').split("=")[0] == 'CONSULTAS': 
			queries_file = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'RESULTADOS': 
			results_file = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'LISTA_TERMOS': 
			term_list_file = row.replace('\n','').split("=")[1]

logging.info('Config file BUSCA.cfg read in: %0.3fs' % (time() - t0))


########################################
## Read QUERIES file
########################################
t0 = time()
df_queries = pd.read_csv(queries_file, header=None, delimiter=';')

logging.info('Queries file read in: %0.3fs' % (time() - t0))

########################################
## Read Term x Doc matrix
########################################
t0 = time()
df_model = pd.read_csv(model_file, header=None, delimiter=';')

logging.info('Term x Doc matrix file read in: %0.3fs' % (time() - t0))

########################################
## Read Term list
########################################
t0 = time()
term_df = pd.read_csv(term_list_file, header=None, delimiter=';')
term_list = term_df.iloc[0].values.tolist()

logging.info('Term list file read in: %0.3fs' % (time() - t0))

########################################
## Queries data frame to vec
########################################
t0 = time()
queries_vec = dict()	
for index, row in df_queries.iterrows():
	query_number = row[0]		
	tokens = nltk.word_tokenize(row[1])
	tokens = [token.upper() for token in tokens if token.isalpha()]
	score_vector = create_score_list(tokens, term_list)
	queries_vec[query_number] = score_vector

logging.info('Query vector generated in: %0.3fs' % (time() - t0))

########################################
## Cosine similarity for all docs
########################################
t0 = time()
rankings = dict()
for query_number, scores in tqdm(queries_vec.items(), desc='Computing cosine similatiry') :
	cos_sim = dict()
	for index, row in df_model.iterrows():			
		cos_sim[row[0]] = similaridadeCosseno(scores, row[1].replace('[','').replace(']','').split(','))
	
	rankings[query_number] = sorted(cos_sim.items(), key=operator.itemgetter(1), reverse=True)

logging.info('Cosine similarity computed in: %0.3fs' % (time() - t0))

########################################
## Generating output
########################################
t0 = time()
output = dict()
for query, ranking in tqdm(rankings.items(), desc='Writing output csv file') :
	i = 0
	itemList = []
	for item in ranking :  
		doc = item[0]
		similarity = item[1]			
		if(similarity != 0) : 
			itemList.append([i, doc, similarity])
			i += 1
	output[query] = itemList
with(open(results_file,"w")) as f:
		writer = csv.writer(f,delimiter=';')
		for i, v in output.items():
			if v != [] :
				writer.writerow([i,v])
logging.info('Output file writen in: %0.3fs' % (time() - t0))
