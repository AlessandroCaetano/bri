#!/usr/bin/python3

########################################
## import packages
########################################
import csv

import logging

from tqdm import tqdm
from time import time

from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from nltk.stem.porter import PorterStemmer
from nltk.corpus.reader import XMLCorpusView
########################################
## Configurations
########################################
logging.basicConfig(filename='inverted_list.log',level=logging.DEBUG)

stemmer = PorterStemmer()
tokenizer = RegexpTokenizer(r'\w+')
STOP_WORDS = set(stopwords.words('english'))

########################################
## reading configuration file
########################################
t0 = time()
with open('GLI.cfg') as f:
	for row in f.readlines():
		if row.replace('\n','').split("=")[0] == 'LEIA_74': 
			cf74 = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'LEIA_75': 
			cf75 = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'LEIA_76': 
			cf76 = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'LEIA_77': 
			cf77 = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'LEIA_78': 
			cf78 = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'LEIA_79': 
			cf79 = row.replace('\n','').split("=")[1]
		if row.replace('\n','').split("=")[0] == 'ESCREVA': 
			tfidf_file = row.replace('\n','').split("=")[1]

logging.info('Config file GLI.cfg read in: %0.3fs' % (time() - t0))

########################################
## read all files records
########################################
t0 = time()
CF74 = XMLCorpusView(cf74, 'FILE/RECORD')
CF75 = XMLCorpusView(cf75, 'FILE/RECORD')
CF76 = XMLCorpusView(cf76, 'FILE/RECORD')
CF77 = XMLCorpusView(cf77, 'FILE/RECORD')
CF78 = XMLCorpusView(cf78, 'FILE/RECORD')
CF79 = XMLCorpusView(cf79, 'FILE/RECORD')
logging.info('XML Files parsed in: %0.3fs' % (time() - t0))

########################################
## merge records into one list
########################################
RECORDS = CF74 + CF75 + CF76 + CF77 + CF78 + CF79


docs_tokens = dict()
tfidf = dict()

########################################
## generates idf 
## Stay Awhile and Listen, takes a lot of time
########################################
t0 = time()
for record in tqdm(RECORDS, desc='Generating Inverted List'):
    ## Checks if record has abstract and tokenize words
	if record.find('ABSTRACT') != None:
		tokens = tokenizer.tokenize(str(record.find('ABSTRACT').text.upper()))
		stopped_tokens = [word for word in tokens if not word in STOP_WORDS]
		stemmed_tokens = [stemmer.stem(word).upper() for word in stopped_tokens]
		record_num = record.find('RECORDNUM').text.replace(' ', '')
		docs_tokens[record_num] = stopped_tokens

	## Checks if record has extract and tokenize words
	elif record.find('EXTRACT') != None:
		tokens = tokenizer.tokenize(str(record.find('EXTRACT').text.upper()))
		stopped_tokens = [word for word in tokens if not word in STOP_WORDS]
		stemmed_tokens = [stemmer.stem(word).upper() for word in stopped_tokens]
		record_num = record.find('RECORDNUM').text.replace(' ', '')
		docs_tokens[record_num] = stopped_tokens

	## Creates IDF list
	for token in stopped_tokens:
		for document in docs_tokens:
			if token in docs_tokens[document]:
				if token in tfidf:
					tfidf[token].append(document)
				else:
					new_list = list()
					new_list.append(document)
					tfidf[token] = new_list

	## Write CSV file
	with(open(tfidf_file,"w")) as f:
		writer = csv.writer(f,delimiter=';')
		for index, value in tfidf.items():
			if value != [] :
				writer.writerow([index,value])

logging.info('TF_IDF Generated in: %0.3fs' % (time() - t0))
