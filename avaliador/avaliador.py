#!/usr/bin/python3

########################################
## import packages
########################################
import csv

import logging
import random
import pandas as pd

from tqdm import tqdm
from time import time

from sklearn.metrics import * 
from sklearn.preprocessing import MultiLabelBinarizer

from nltk.corpus.reader import XMLCorpusView
import matplotlib.pyplot as plt

CF74 = XMLCorpusView('../data/cf74.xml', 'FILE/RECORD/RECORDNUM')
CF75 = XMLCorpusView('../data/cf75.xml', 'FILE/RECORD/RECORDNUM')
CF76 = XMLCorpusView('../data/cf76.xml', 'FILE/RECORD/RECORDNUM')
CF77 = XMLCorpusView('../data/cf77.xml', 'FILE/RECORD/RECORDNUM')
CF78 = XMLCorpusView('../data/cf78.xml', 'FILE/RECORD/RECORDNUM')
CF79 = XMLCorpusView('../data/cf79.xml', 'FILE/RECORD/RECORDNUM')
QUERIES = XMLCorpusView('../data/cfquery.xml', 'FILEQUERY/QUERY')

truth = []
for query in QUERIES:
    query_truth = []
    for result in query.find('Records'):
        query_truth.append(int(result.text))
    truth.append(query_truth)

ALL_DOCS = CF74 + CF75 + CF76 + CF77 + CF78 + CF79
ALL_CLASSES = [int(doc.text) for doc in ALL_DOCS]

def precision_at_k(predicted, k):
    for index, vec in enumerate(predicted):
        if len(vec) > k:
            predicted[index] = vec[:k]
    return predicted

df_queries = pd.read_csv('../buscador/resultados.csv', header=None, delimiter=';')
df_queries = df_queries.sort_values(0)

predicted = []

for row in df_queries[1]:
    query_prediction = []
    row = eval(row)
    for vec in row:
        query_prediction.append(int(vec[1]))
    predicted.append(query_prediction)

predicted_at_5 = precision_at_k(predicted,5)
predicted_at_10 = precision_at_k(predicted,10)

binarizer = MultiLabelBinarizer(classes=ALL_CLASSES)
y_true = binarizer.fit_transform(truth)
y_pred = binarizer.transform(predicted)
y_pred_at_5 = binarizer.transform(predicted_at_5)
y_pred_at_10 = binarizer.transform(predicted_at_10)
print('Precision score by samples: {0:0.4f}'.format(precision_score(y_true, y_pred, average='samples')))
print('Precision at 5 score by samples: {0:0.4f}'.format(precision_score(y_true, y_pred_at_5, average='samples')))
print('Precision at 10 score by samples: {0:0.4f}'.format(precision_score(y_true, y_pred_at_10, average='samples')))
print('Recall score by samples: {0:0.4f}'.format(recall_score(y_true, y_pred, average='samples')))
print('F1 score by samples: {0:0.4f}'.format(f1_score(y_true, y_pred, average='samples')))
print('Average precision score averaged by samples: {0:0.4f}'.format(average_precision_score(y_true, y_pred, average='samples')))

recall = dict()
precision = dict()
average_precision = dict()

for index in range(len(ALL_CLASSES)):
    if(index == ALL_CLASSES):
        break
    else:
        precision[index], recall[index], _ = precision_recall_curve(y_true[:,index],y_pred[:,index])
        average_precision[index] = average_precision_score(y_true[:,index], y_pred[:, index])

precision["micro"], recall["micro"], _ = precision_recall_curve(y_true.ravel(), y_pred.ravel(), pos_label=1)
average_precision["micro"] = average_precision_score(y_true, y_pred, average="micro")


graph_precision = []
graph_recall = []
for element in recall.values():
    for value in element:
        if value >= 0.01 and value <= 0.999:
            graph_recall.append(value)


for element in precision.values():
    for value in element:
        if value >= 0.01 and value <= 0.999:
            graph_precision.append(value)


graph_precision.insert(0, 0.0)
graph_precision.insert(len(graph_precision), 1.0)
graph_precision.sort()

graph_recall.insert(0, 0.0)
graph_recall.insert(len(graph_precision), 1.0)
graph_recall.sort()
diff = len(graph_precision) - len(graph_recall)

for i in range(0, diff):
    graph_recall.insert(0,random.uniform(0, 1.0))
graph_recall.sort(reverse=True)

plt.figure()
plt.step(graph_recall, graph_precision, color='b', alpha=0.2,
         where='post')
plt.fill_between(graph_recall, graph_precision, step='post', alpha=0.2,
                 color='b')

plt.xlabel('Recall')
plt.ylabel('Precision')
plt.ylim([0.0, 1.05])
plt.xlim([0.0, 1.0])
plt.title('Average precision score')
plt.show()